import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/header/header.component';
import { FooterComponent } from './shared/footer/footer.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './pages/dashboard/material.module';
import { HttpClientModule } from '@angular/common/http';
import { LoginComponent } from './pages/dashboard/login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { tokenInterceptorProvider } from './services/token-interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        MaterialModule,
        ReactiveFormsModule
    ],
  providers: tokenInterceptorProvider,
  bootstrap: [AppComponent]
})
export class AppModule {
}
