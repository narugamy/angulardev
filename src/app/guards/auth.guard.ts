import { Injectable } from '@angular/core';
import { CanActivate, CanLoad, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from '../services/login.service';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad {

  constructor(private loginService: LoginService,
              private router: Router) {
  }

  response(): boolean {
    const token = this.loginService.Token;
    if ( token === '' || token === null ) {
      this.router.navigateByUrl('dashboard/login');
      return false;
    }
    return true;
  }

  canActivate(): Observable<boolean> | boolean {
    return this.response();
  }

  canLoad(): Observable<boolean> | boolean {
    return this.response();
  }

}
