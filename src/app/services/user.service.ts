import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private baseUrl: string = environment.url;

  constructor(private http: HttpClient) {
  }

  params(): Observable<any> {
    return this.http.get(`${ this.baseUrl }user/params`);
  }

  getAll(page = 1, httpParams: any): Observable<any> {
    httpParams.page = page;
    return this.http.get(`${ this.baseUrl }user`, { params: httpParams });
  }

  find(id: number): Observable<any> {
    return this.http.get(`${ this.baseUrl }user/show/${ id }`);
  }

  save(user: any): Observable<any> {
    return this.http.post(`${ this.baseUrl }user/save`, user);
  }

  update(user: any): Observable<any> {
    return this.http.put(`${ this.baseUrl }user/show/${ user.id }`, user);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${ this.baseUrl }user/delete/${ id }`);
  }

}
