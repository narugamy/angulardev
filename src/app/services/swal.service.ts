import { Injectable } from '@angular/core';
import Swal, { SweetAlertIcon, SweetAlertResult } from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SwalService {

  constructor() {
  }

  getSwal(title: string, icons: SweetAlertIcon = 'warning', text: string = '', cancelShow: boolean = true): Promise<SweetAlertResult<any>> {
    return Swal.fire({
      title: `${ title }`,
      text: `${ text }`,
      icon: icons,
      showCancelButton: cancelShow,
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar',
    });
  }

}
