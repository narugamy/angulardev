import { Injectable } from '@angular/core';
import { HTTP_INTERCEPTORS, HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { LocalstorageService } from './localstorage.service';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { SwalService } from './swal.service';

@Injectable({
  providedIn: 'root'
})
export class TokenInterceptorService implements HttpInterceptor {

  constructor(private localstorageService: LocalstorageService, private router: Router, private swalService: SwalService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token: string = this.localstorageService.get('token');
    let request = req;
    if ( token ) {
      request = req.clone({
        setHeaders: {
          authorization: `Bearer ${ token }`
        }
      });
    }

    return next.handle(request).pipe(
      catchError((err: HttpErrorResponse) => {
        if ( err.status === 401 ) {
          this.localstorageService.remove('token');
          this.localstorageService.remove('user');
          this.localstorageService.remove('image');
          this.router.navigateByUrl('/dashboard/login');
        }
        return throwError(err);
      })
    );
  }
}

export const tokenInterceptorProvider = [
  { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptorService, multi: true }
];
