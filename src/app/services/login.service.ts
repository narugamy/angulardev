import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { LocalstorageService } from './localstorage.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private baseUrl: string = environment.url;
  private user!: any;
  private token!: string;
  private image!: any;

  constructor(private http: HttpClient, private localstorageService: LocalstorageService) {
  }

  get User(): any {
    const item = this.user || this.localstorageService.get('user');
    return item;
  }

  get Token(): any {
    return this.token || this.localstorageService.get('token');
  }

  get Image(): any {
    return this.image || this.localstorageService.get('image');
  }

  login(user: any): Observable<any> {
    return this.http.post(`${ this.baseUrl }login`, user)
    .pipe(
      tap((response: any) => {
        if ( response.resp ) {
          this.user = response.user;
        }
      })
    );
  }

  update(user: any): Observable<any> {
    return this.http.post(`${ this.baseUrl }login/update`, user);
  }

  upload(img: any): Observable<any> {
    return this.http.post(`${ this.baseUrl }login/image`, img);
  }

}
