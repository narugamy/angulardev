import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RolService {

  private baseUrl: string = environment.url;

  constructor(private http: HttpClient) {
  }

  params(): Observable<any> {
    return this.http.get(`${ this.baseUrl }rol/params`);
  }

  getAll(page = 1, httpParams: any): Observable<any> {
    httpParams.page = page;
    return this.http.get(`${ this.baseUrl }rol`, { params: httpParams });
  }

  find(id: number): Observable<any> {
    return this.http.get(`${ this.baseUrl }rol/show/${ id }`);
  }

  save(user: any): Observable<any> {
    return this.http.post(`${ this.baseUrl }rol/save`, user);
  }

  update(user: any): Observable<any> {
    return this.http.put(`${ this.baseUrl }rol/show/${ user.id }`, user);
  }

  delete(id: number): Observable<any> {
    return this.http.delete(`${ this.baseUrl }rol/delete/${ id }`);
  }

}
