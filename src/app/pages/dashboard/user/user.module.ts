import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {UserRoutingModule} from './user-routing.module';
import {UserListComponent} from './user-list/user-list.component';
import {UserComponent} from './user.component';
import { MaterialModule } from '../material.module';
import { UserCreateComponent } from './user-create/user-create.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PrimeModule } from '../prime.module';

@NgModule ({
  declarations: [UserListComponent, UserComponent, UserCreateComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    PrimeModule
  ]
})
export class UserModule {
}

