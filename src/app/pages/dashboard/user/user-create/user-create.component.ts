import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../../../../services/user.service';
import { switchMap } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
import { SwalService } from '../../../../services/swal.service';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})
export class UserCreateComponent implements OnInit {

  title = '';
  message = '';
  form: FormGroup;
  userErrors: any;
  roles: any[] = [];

  constructor(private userService: UserService, private activateRoute: ActivatedRoute, private router: Router,
              private formBuilder: FormBuilder, private spinnerService: NgxSpinnerService, private swalService: SwalService) {
    this.form = this.formBuilder.group({
      id: [''],
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      surname: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      email: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      sex: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(1)]],
      date: ['', [Validators.required]],
      rol_id: ['', [Validators.required]],
      deleted_at: [{ value: '1', disabled: true }, [Validators.required, Validators.minLength(1), Validators.maxLength(1)]],
    });
    this.resetError();
  }

  save(): void {
    if ( this.form.invalid ) {
      this.form.markAllAsTouched();
      return;
    }
    let options: any = { title: this.message, icons: 'warning', text: '', };
    this.swalService.getSwal(options.title, options.icons, options.text)
    .then((result) => {
      if ( result.value ) {
        this.resetError();
        this.spinner();
        let service: Observable<any>;
        if ( this.form.controls.id.value ) {
          service = this.userService.update(this.form.value);
        } else {
          service = this.userService.save(this.form.value);
        }
        service.subscribe(resp => {
          options = { title: resp.message, icons: 'info', text: '', cancelShow: false };
          this.spinnerService.hide();
          this.swalService.getSwal(options.title, options.icons, options.text, options.cancelShow)
          .then(() => {
            this.form.reset();
            this.router.navigate(['/dashboard/user']);
          });
        }, (error) => {
          if ( error.status === 422 ) {
            for ( const item of Object.keys(error.error.errors) ) {
              this.userErrors[item] = error.error.errors[item];
              this.form.controls[item].setErrors({ incorrect: true });
            }
          }
          options = { title: 'Errores encontrados', icons: 'error', text: '', cancelShow: false };
          this.spinnerService.hide();
          this.swalService.getSwal(options.title, options.icons, options.text, options.cancelShow)
          .then(() => {
          });
        });
      } else {
        console.log('cancelado');
      }
    });
  }

  ngOnInit(): void {
    this.spinner();
    this.userService.params().subscribe(res => {
      this.roles = res.roles;
    });
    if ( !this.router.url.includes('show') ) {
      this.message = 'Desea crear el usuario';
      this.title = 'Crear nuevo usuario';
      this.spinnerHide();
      return;
    }
    this.activateRoute.params
    .pipe(switchMap(({ id }) => this.userService.find(id)))
    .subscribe((res) => {
      this.title = `Actualizar usuario: ${ res.user.name }`;
      this.message = `Desea actualizar el usuario: ${ res.user.name }`;
      this.form.reset({
        id: res.user.id,
        name: res.user.name,
        surname: res.user.surname,
        email: res.user.email,
        sex: res.user.sex,
        rol_id: res.user.rol_id,
        date: `${ res.user.date }T00:00:00`,
        deleted_at: ( res.user.deleted_at === null ) ? '1' : '2'
      });
      // this.form.get('rol_id')?.disable();
      this.spinnerHide();
    });
  }

  spinner(): void {
    this.spinnerService.show();
  }

  spinnerHide(): void {
    setTimeout(() => {
      this.spinnerService.hide();
    }, 2000);
  }

  resetError(): void {
    this.userErrors = {
      name: [],
      surname: [],
      sex: [],
      date: [],
      rol_id: [],
      email: [],
    };
  }

}
