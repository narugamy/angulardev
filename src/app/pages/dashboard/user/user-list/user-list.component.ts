import { Component, OnInit } from '@angular/core';
import { User } from '../../../../interfaces/user';
import { UserService } from '../../../../services/user.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { SwalService } from '../../../../services/swal.service';
import { LazyLoadEvent } from 'primeng/api';
import { Table } from 'primeng/table';


@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  providers: [UserService]
})
export class UserListComponent implements OnInit {

  users: User[] = [];
  totalRecords: number;
  loading: boolean;
  pagination: number;
  filters: any = {
    page: 0,
    name: '',
    surname: '',
    email: '',
    status: '',
  };
  statues: any[] = [
    { label: 'Activo', value: 1 },
    { label: 'Inactivo', value: 2 },
  ];

  constructor(private userService: UserService, private spinnerService: NgxSpinnerService, private swalService: SwalService) {
    this.totalRecords = 0;
    this.loading = true;
    this.pagination = 1;
  }

  ngOnInit(): void {
    this.userService.params().subscribe(res => {
      console.log(res);
    });
  }

  loadCustomers(event: LazyLoadEvent): void {
    this.loading = true;
    this.pagination = ( ( event.first ?? 0 ) / 2 ) + 1;
    this.loadFilter(event.filters);
    setTimeout(() => {
      this.userService.getAll(this.pagination, this.filters).subscribe(res => {
        this.users = res.paginate.data ?? [];
        this.totalRecords = res.paginate.total ?? 0;
        this.loading = false;
      });
    }, 1000);
  }

  delete(user: any): void {
    const message = ( user.deleted_at === null ) ? 'desactivar' : 'activar';
    let options: any = { title: `¿Desea ${ message } el usuario?`, icons: 'warning', text: '', };
    this.swalService.getSwal(options.title, options.icons, options.text)
    .then((result) => {
      if ( result.value ) {
        this.spinnerService.show();
        this.userService.delete(user.id).subscribe((resp) => {
          this.spinnerService.hide();
          options = { title: resp.message, icons: 'info', text: '', cancelShow: false };
          this.swalService.getSwal(options.title, options.icons, options.text, options.cancelShow).then(() => {
            this.spinnerService.show();
            this.userService.getAll(this.pagination, this.filters).subscribe((res) => {
              this.users = res.paginate.data ?? [];
              this.totalRecords = res.paginate.total ?? 0;
              this.loading = false;
              this.spinnerService.hide();
            });
          });
        });
      } else {
        console.log('cancelado');
      }
    });
  }

  clear(table: Table): void {
    this.filters = {
      page: 0,
      name: '',
      surname: '',
      email: '',
      status: '',
    };
    table.clear();
  }

  loadFilter(filters: any): void {
    for ( const key in filters ) {
      if ( filters.hasOwnProperty(key) ) {
        this.filters[key] = '';
        if ( filters[key].value ) {
          this.filters[key] = filters[key].value;
        }
      } else {
        this.filters[key] = '';
      }
    }
  }

}
