import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutComponent } from './layout.component';

const routes: Routes = [
  {
    path: '', component: LayoutComponent, children: [
      {
        path: 'user', loadChildren: () => import('../user/user.module').then(mod => mod.UserModule)
      },
      {
        path: 'rol', loadChildren: () => import('../rol/rol.module').then(mod => mod.RolModule)
      },
      {
        path: 'perfil', loadChildren: () => import('../perfil/perfil.module').then(mod => mod.PerfilModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LayoutRoutingModule {
}
