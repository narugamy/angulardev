import { Component, OnInit } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { LoginService } from '../../../services/login.service';
import { LocalstorageService } from '../../../services/localstorage.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

  opened: boolean;

  constructor(private loginService: LoginService, private localstorageService: LocalstorageService, private router: Router) {
    this.opened = true;
  }

  get user(): any {
    return this.loginService.User;
  }

  get image(): any {
    return this.loginService.Image;
  }

  logout(): void {
    this.localstorageService.remove('token');
    this.localstorageService.remove('user');
    this.localstorageService.remove('image');
    this.router.navigateByUrl('/dashboard/login');
  }

  shower(item: MatSidenav): void {
    const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    if ( width <= 880 ) {
      item.toggle();
    }
  }

  close(response: boolean = true, element: any): void {
    if ( response ) {
      element.toggle();
    }
  }

  ngOnInit(): void {
  }

  isLargeScreen(): boolean {
    const width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    if ( width > 1120 ) {
      return true;
    } else {
      this.opened = false;
      return false;
    }
  }

  onResize(event: any): void {
    const width = event.target.innerWidth;
    this.opened = width > 880;
  }

}
