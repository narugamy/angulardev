import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { SwalService } from '../../../../services/swal.service';
import { LoginService } from '../../../../services/login.service';
import { HttpClient } from '@angular/common/http';
import { LocalstorageService } from '../../../../services/localstorage.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent implements OnInit {

  title = '';
  message = '';
  form: FormGroup;
  userErrors: any;
  image: any;
  options: any = { title: this.message, icons: 'warning', text: '' };
  @ViewChild('fileUpload') fileUpload: any;

  constructor(private activateRoute: ActivatedRoute, private router: Router,
              private formBuilder: FormBuilder, private spinnerService: NgxSpinnerService, private swalService: SwalService,
              private loginService: LoginService, private localstorageService: LocalstorageService) {
    this.form = this.formBuilder.group({
      id: [''],
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      surname: ['', [Validators.minLength(3), Validators.maxLength(255)]],
      sex: ['', [Validators.required, Validators.minLength(1), Validators.maxLength(1)]],
      date: ['', [Validators.required]],
      email: [{ value: '', disabled: true }, []],
      deleted_at: [{ value: '1', disabled: true }, [Validators.required, Validators.minLength(1), Validators.maxLength(1)]],
    });
    this.resetError();
  }

  onUpload(event: any): void {
    const formData: FormData = new FormData();
    formData.append('image', event.files[0]);
    formData.append('token', this.loginService.Token);
    this.loginService.upload(formData).subscribe(resp => {
      this.localstorageService.set('user', resp.user);
      this.localstorageService.set('image', resp.image);
      this.fileUpload.clear();
      this.options = { title: resp.message, icons: 'info', text: '', cancelShow: false };
      this.spinnerService.hide();
      this.swalService.getSwal(this.options.title, this.options.icons, this.options.text, this.options.cancelShow)
      .then(() => {
        this.resetError();
      });
    }, (error) => {
      console.log(error);
    });
  }

  update(): void {
    if ( this.form.invalid ) {
      this.form.markAllAsTouched();
      return;
    }
    this.options = { title: `Actualizar perfil`, icons: 'warning', text: `¿Desea actualizar su perfil?`, cancelShow: false };
    this.swalService.getSwal(this.options.title, this.options.icons, this.options.text)
    .then((result) => {
      if ( result.value ) {
        this.resetError();
        this.spinner();
        this.loginService.update(this.form.value).subscribe(resp => {
          this.localstorageService.set('user', resp.user);
          this.options = { title: resp.message, icons: 'info', text: '', cancelShow: false };
          this.spinnerService.hide();
          this.swalService.getSwal(this.options.title, this.options.icons, this.options.text, this.options.cancelShow)
          .then(() => {
            this.resetError();
          });
        }, (error) => {
          if ( error.status === 422 ) {
            for ( const item of Object.keys(error.error.errors) ) {
              this.userErrors[item] = error.error.errors[item];
              this.form.controls[item].setErrors({ incorrect: true });
            }
          }
          this.options = { title: 'Errores encontrados', icons: 'error', text: '', cancelShow: false };
          this.spinnerService.hide();
          this.swalService.getSwal(this.options.title, this.options.icons, this.options.text, this.options.cancelShow)
          .then(() => {
          });
        });
      } else {
        console.log('cancelado');
      }
    });
  }

  ngOnInit(): void {
    const user = this.loginService.User;
    this.form.reset({
      id: user.id,
      name: user.name,
      surname: user.surname,
      sex: user.sex,
      email: user.email,
      date: `${ user.date }T00:00:00`,
      deleted_at: ( user.deleted_at === null ) ? '1' : '2'
    });
    this.spinnerHide();
    this.title = `Actualizar perfil`;
    this.message = `¿Desea actualizar su perfil?`;
  }

  spinner(): void {
    this.spinnerService.show();
  }

  spinnerHide(): void {
    setTimeout(() => {
      this.spinnerService.hide();
    }, 2000);
  }

  resetError(): void {
    this.userErrors = {
      name: [],
      surname: [],
      sex: [],
      date: [],
      email: [],
    };
  }

}
