import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InfoComponent } from './info/info.component';
import { PerfilComponent } from './perfil.component';

const routes: Routes = [
  {
    path: '', component: PerfilComponent, children: [
      {
        path: 'info', component: InfoComponent
      },
      {
        path: '**', redirectTo: 'info', pathMatch: 'full'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PerfilRoutingModule { }
