import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PerfilRoutingModule } from './perfil-routing.module';
import { InfoComponent } from './info/info.component';
import { PerfilComponent } from './perfil.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';
import { PrimeModule } from '../prime.module';


@NgModule({
  declarations: [InfoComponent, PerfilComponent],
  imports: [
    CommonModule,
    PerfilRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    PrimeModule
  ],
})
export class PerfilModule {
}
