import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { RolRoutingModule } from './rol-routing.module';
import { RolComponent } from './rol.component';
import { RolCreateComponent } from './rol-create/rol-create.component';
import { RolListComponent } from './rol-list/rol-list.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';
import { PrimeModule } from '../prime.module';


@NgModule({
  declarations: [RolComponent, RolCreateComponent, RolListComponent],
  imports: [
    CommonModule,
    RolRoutingModule,
    ReactiveFormsModule,
    MaterialModule,
    PrimeModule
  ]
})
export class RolModule {
}
