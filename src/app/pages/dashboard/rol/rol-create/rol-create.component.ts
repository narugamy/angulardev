import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { RolService } from '../../../../services/rol.service';
import { switchMap } from 'rxjs/operators';
import { NgxSpinnerService } from 'ngx-spinner';
import { Observable } from 'rxjs';
import { SwalService } from '../../../../services/swal.service';

@Component({
  selector: 'app-rol-create',
  templateUrl: './rol-create.component.html',
  styleUrls: ['./rol-create.component.scss']
})
export class RolCreateComponent implements OnInit {

  title = '';
  message = '';
  rolErrors: any;
  form: FormGroup;

  constructor(private rolService: RolService, private activateRoute: ActivatedRoute, private router: Router,
              private formBuilder: FormBuilder, private spinnerService: NgxSpinnerService, private swalService: SwalService) {
    this.form = this.formBuilder.group({
      id: [''],
      name: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]]
    });
    this.resetError();
  }

  save(): void {
    if ( this.form.invalid ) {
      this.form.markAllAsTouched();
      return;
    }
    let options: any = { title: this.message, icons: 'warning', text: '', };
    this.swalService.getSwal(options.title, options.icons, options.text)
    .then((result) => {
      if ( result.value ) {
        this.resetError();
        this.spinner();
        let service: Observable<any>;
        if ( this.form.controls.id.value ) {
          service = this.rolService.update(this.form.value);
        } else {
          service = this.rolService.save(this.form.value);
        }
        service.subscribe((resp) => {
          options = { title: resp.message, icons: 'info', text: '', cancelShow: false };
          this.spinnerService.hide();
          this.swalService.getSwal(options.title, options.icons, options.text, options.cancelShow)
          .then(() => {
            this.form.reset();
            this.router.navigate(['/dashboard/rol']);
          });
        }, (error) => {
          if ( error.status === 422 ) {
            for ( const item of Object.keys(error.error.errors) ) {
              this.rolErrors[item] = error.error.errors[item];
              this.form.controls[item].setErrors({ incorrect: true });
            }
          }
          options = { title: 'Errores encontrados', icons: 'error', text: '', cancelShow: false };
          this.spinnerService.hide();
          this.swalService.getSwal(options.title, options.icons, options.text, options.cancelShow)
          .then(() => {
          });
        });
      } else {
        this.spinnerService.hide();
        console.log('cancelado');
      }
    });
  }

  ngOnInit(): void {
    if ( !this.router.url.includes('show') ) {
      this.message = 'Desea crear el rol';
      this.title = 'Crear nuevo rol';
      this.spinnerHide();
      return;
    }
    this.spinner();
    this.activateRoute.params
    .pipe(switchMap(({ id }) => this.rolService.find(id)))
    .subscribe((resp) => {
      const rol = resp.rol;
      this.title = `Actualizar rol: ${ rol.name }`;
      this.message = `Desea actualizar el rol: ${ rol.name }`;
      this.form.reset({ id: rol.id, name: rol.name });
      this.spinnerHide();
    }, (err) => {
      this.spinnerService.hide();
      if ( err.status === 404 ) {
        const options: any = { title: 'No se puede cominicar con el servidor', icons: 'error', text: '', cancelShow: false };
        this.swalService.getSwal(options.title, options.icons, options.text, options.cancelShow)
        .then((result) => {
          if ( result.value ) {
            this.router.navigate(['/dashboard/rol']);
          }
        });
      }
    });
  }

  spinner(): void {
    this.spinnerService.show();
  }

  spinnerHide(): void {
    setTimeout(() => {
      this.spinnerService.hide();
    }, 1000);
  }

  resetError(): void {
    this.rolErrors = {
      name: []
    };
  }

}
