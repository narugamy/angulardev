import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RolComponent } from './rol.component';
import { RolListComponent } from './rol-list/rol-list.component';
import { RolCreateComponent } from './rol-create/rol-create.component';
const routes: Routes = [
  {
    path: '', component: RolComponent, children: [
      {
        path: 'list', component: RolListComponent
      },
      {
        path: 'create', component: RolCreateComponent
      },
      {
        path: 'show/:id', component: RolCreateComponent
      },
      {
        path: '**', redirectTo: 'list', pathMatch: 'full'
      },
    ]
  }
];
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RolRoutingModule { }
