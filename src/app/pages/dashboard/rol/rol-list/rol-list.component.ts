import { Component, OnInit } from '@angular/core';
import { RolService } from '../../../../services/rol.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { LazyLoadEvent } from 'primeng/api';
import { SwalService } from '../../../../services/swal.service';
import { Table } from 'primeng/table';

@Component({
  selector: 'app-rol-list',
  templateUrl: './rol-list.component.html',
  styleUrls: ['./rol-list.component.scss']
})
export class RolListComponent implements OnInit {

  rols: any[] = [];
  totalRecords: number;
  loading: boolean;
  pagination: number;
  filters: any = {
    page: 0,
    name: '',
    status: '',
  };
  statues: any[] = [
    { label: 'Activo', value: 1 },
    { label: 'Inactivo', value: 2 },
  ];

  constructor(private rolService: RolService, private spinnerService: NgxSpinnerService, private swalService: SwalService) {
    this.totalRecords = 0;
    this.loading = true;
    this.pagination = 1;
  }

  ngOnInit(): void {
  }

  loadCustomers(event: LazyLoadEvent): void {
    this.loading = true;
    this.pagination = ( ( event.first ?? 0 ) / 2 ) + 1;
    this.loadFilter(event.filters);
    setTimeout(() => {
      this.rolService.getAll(this.pagination, this.filters).subscribe(res => {
        this.rols = res.paginate.data ?? [];
        this.totalRecords = res.paginate.total ?? 0;
        this.loading = false;
      });
    }, 1000);
  }

  delete(rol: any): void {
    const message = ( rol.deleted_at === null ) ? 'desactivar' : 'activar';
    let options: any = { title: `¿Desea ${ message } el usuario?`, icons: 'warning', text: '', };
    this.swalService.getSwal(options.title, options.icons, options.text)
    .then((result) => {
      if ( result.value ) {
        this.spinnerService.show();
        this.rolService.delete(rol.id).subscribe((resp) => {
          this.spinnerService.hide();
          options = { title: resp.message, icons: 'info', text: '', cancelShow: false };
          this.swalService.getSwal(options.title, options.icons, options.text, options.cancelShow)
          .then(() => {
            this.spinnerService.show();
            this.rolService.getAll(this.pagination, this.filters).subscribe((res) => {
              this.rols = res.paginate.data ?? [];
              this.totalRecords = res.paginate.total ?? 0;
              this.loading = false;
              this.spinnerService.hide();
            });
          });
        }, (error) => {
          options = { title: error.error.message ?? 'Ocurrio un error', icons: 'error', text: '', cancelShow: false };
          this.spinnerService.hide();
          this.swalService.getSwal(options.title, options.icons, options.text, options.cancelShow)
          .then(() => {
          });
        });
      } else {
        console.log('cancelado');
      }
    });
  }

  clear(table: Table): void {
    this.filters = {
      page: 0,
      name: '',
      status: '',
    };
    table.clear();
  }

  loadFilter(filters: any): void {
    for ( const key in filters ) {
      if ( filters.hasOwnProperty(key) ) {
        this.filters[key] = '';
        if ( filters[key].value ) {
          this.filters[key] = filters[key].value;
        }
      } else {
        this.filters[key] = '';
      }
    }
  }

}
