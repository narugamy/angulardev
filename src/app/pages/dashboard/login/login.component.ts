import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { SwalService } from '../../../services/swal.service';
import { Observable } from 'rxjs';
import { LoginService } from '../../../services/login.service';
import { LocalstorageService } from '../../../services/localstorage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  message = 'Iniciar sesion';
  rolErrors: any;
  form: FormGroup;

  constructor(private loginService: LoginService, private activateRoute: ActivatedRoute, private router: Router,
              private formBuilder: FormBuilder, private spinnerService: NgxSpinnerService, private swalService: SwalService,
              private localstorageService: LocalstorageService) {
    this.form = this.formBuilder.group({
      email: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]],
      password: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(50)]]
    });
    this.resetError();
  }

  ngOnInit(): void {
  }

  save(): void {
    if ( this.form.invalid ) {
      this.form.markAllAsTouched();
      return;
    }
    let options: any = { title: this.message, icons: 'warning', text: '', };
    this.swalService.getSwal(options.title, options.icons, options.text)
    .then((result) => {
      if ( result.value ) {
        this.resetError();
        this.spinner();
        let service: Observable<any>;
        service = this.loginService.login(this.form.value);
        service.subscribe((resp) => {
          options = { title: resp.message, icons: 'info', text: '', cancelShow: false };
          this.localstorageService.set('token', resp.token);
          this.localstorageService.set('user', resp.user);
          this.localstorageService.set('image', resp.image);
          this.spinnerService.hide();
          this.swalService.getSwal(options.title, options.icons, options.text, options.cancelShow)
          .then(() => {
            this.form.reset();
            this.router.navigateByUrl('/dashboard');
          });
        }, (error) => {
          if ( error.status === 422 ) {
            for ( const item of Object.keys(error.error.errors) ) {
              this.rolErrors[item] = error.error.errors[item];
              this.form.controls[item].setErrors({ incorrect: true });
            }
          }
          options = { title: 'Errores encontrados', icons: 'error', text: error.error.message, cancelShow: false };
          this.spinnerService.hide();
          this.swalService.getSwal(options.title, options.icons, options.text, options.cancelShow)
          .then(() => {});
        });
      } else {
        this.spinnerService.hide();
      }
    });
  }

  spinner(): void {
    this.spinnerService.show();
  }

  spinnerHide(): void {
    setTimeout(() => {
      this.spinnerService.hide();
    }, 1000);
  }

  resetError(): void {
    this.rolErrors = {
      email: [],
      password: []
    };
  }

}
