import {Rol} from './rol';

export interface User {
  id?: number;
  name?: string;
  surname?: string;
  sex?: string;
  date?: string;
  email?: string;
  deleted_at?: string;
}
